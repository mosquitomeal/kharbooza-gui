![image info](data/icon.png)
# Kharbooza GUI
A graphical user interface for creating, solving and analysing quizzes and surveys, based on the wonders of the free and open-source library Kharbooza!
